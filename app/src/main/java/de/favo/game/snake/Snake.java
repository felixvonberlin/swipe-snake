package de.favo.game.snake;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.util.Random;
import java.util.Vector;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Snake{

    static final int NORTH = 0;
    static final int WEST = 270;
    static final int EAST = 90;
    static final int SOUTH = 180;

    private boolean end = false;
    private int x,y,X,Y,orientation;
    private SnakeEvents events;
    private Vector<SnakePart> parts;
    private GameAttributes attributes;
    private Vector<SnakeEvent> items;
    private Vector<WallBlock> wallBlocks;
    private Random r;
    private long steps = 0;
    private int shouldGrow = 0;
    private int object_size = 128;


    public Snake(GameAttributes attr) {
        this.orientation= NORTH;
        this.x          = attr.getXBlockCount()/2;
        this.y          = attr.getYBlockCount()/2;
        this.X          = attr.getXBlockCount();
        this.Y          = attr.getYBlockCount();
        this.events     = attr.getEvents();
        this.attributes = attr;
        this.parts      = new Vector<>();
        this.items      = new Vector<>();
        this.wallBlocks = new Vector<>();
        this.r           = new Random();
        {
            //generate WALLS

            // check if i have to generate walls
            if (attr.getWall() != 0){

                // simple wall (vertical)
                if (attr.getWall() == 1){
                    int length = (int) ((double)Y * 0.8d);
                    int padding = (Y - length);
                    for (int ypsilon = padding; ypsilon < length; ypsilon++)
                        // x -1 because the snake "lives" on x
                        wallBlocks.add(new WallBlock(x-1,ypsilon));
                    {
                        //add snake ass
                        x = (int)((double)X * 0.75d);
                        //y is fine

                        parts.add(new SnakePart(x,y+1,NORTH));
                        parts.add(new SnakePart(x,y,NORTH));
                    }
                }
                // simple wall (horizontal)
                if (attr.getWall() == 2){
                    int length = (int) ((double)X * 0.8d);
                    int padding = (X - length);
                    for (int xs = padding; xs < length; xs++)
                        // y+2 because the snake "lives" on y
                        wallBlocks.add(new WallBlock(xs,y));
                    {
                        // x is fine
                        y = y-3;
                        //add snake ass
                        parts.add(new SnakePart(x,y+1,NORTH));
                        parts.add(new SnakePart(x,y,NORTH));

                    }
                }

            }else {
                //add default snake ass
                parts.add(new SnakePart(x,y+1,NORTH));
                parts.add(new SnakePart(x,y,NORTH));
                // x&y are the start-point > all fine
            }
        }
    }

    int getShouldGrow() {
        return shouldGrow;
    }

    void changeDirection(int direction){
        orientation = direction;
    }

    private Bitmap getEventBitmap(int shouldGrowForColor) {
        Bitmap b = Bitmap.createBitmap(object_size,object_size,
                Bitmap.Config.ARGB_8888);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        Canvas c = new Canvas(b);
        int[] colors = {Color.parseColor("#666666"), Color.parseColor("#cccccc")};
        int third = object_size / 3;
        if (attributes.isUseColorFullMode())
            p.setColor(colors[shouldGrowForColor % 2]);
        else
            p.setColor(attributes.getForegroundColor());
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        c.drawRoundRect(new RectF(10,10,object_size-10,object_size-10),10,10,p);

        p.setColor(attributes.getBackgroundColor());
        c.drawRoundRect(new RectF(third,third,object_size-third,object_size-third),10,10,p);
        return b;
    }

    void goForward(){
        int fX = 0, fY = 0;
        if (orientation == NORTH)
            fY = -1;
        if (orientation == SOUTH)
            fY = 1;
        if (orientation == WEST)
            fX = -1;
        if (orientation == EAST)
            fX = 1;
        if (!checkCoordinates(x+fX,y+fY))
            return;
        if (shouldGrow == 0)
            parts.remove(0);
        else
            shouldGrow--;
        parts.add(new SnakePart(x+fX,y+fY,orientation));
        x = x + fX;
        y = y + fY;
        steps++;
        if (steps % (5+r.nextInt(5)) == 0){
            int x = r.nextInt(X);
            int y = r.nextInt(Y);
            int s = 1 + r.nextInt(2);
            items.add(new SnakeEvent(x,y,s,Bitmap.createScaledBitmap(getEventBitmap(s-1),
                    attributes.getBlockWidth(),
                    attributes.getBlockHeight(),false)));
        }
        events.snakeGetLonger(getActualScore(),parts.size()+shouldGrow);
    }




    private boolean checkCoordinates(int x, int y){
        if (end)
            return false;
        if ((!((x < X) && (x >= 0))) || (!((y < Y) && (y >= 0)))) {
                end = true;
                events.gameOver(getActualScore(),SnakeEvents.SNAKE_LEFT_SCREEN);
                return false;
        }

        if (X*Y == parts.size()) {
            end = true;
            events.gameOver(getActualScore(),SnakeEvents.VIEW_FULL);
            return false;
        }

        for (SnakeEvent se:new Vector<>(items)) {
            if (se.getX() == x && se.getY() == y) {
                shouldGrow = shouldGrow + se.getExtension();
                items.remove(se);
            }
        }

        for (WallBlock w:new Vector<>(wallBlocks)){
            if (w.getX() == x && w.getY() == y) {
                end = true;
                events.gameOver(getActualScore(),SnakeEvents.SNAKE_LEFT_SCREEN);
                return false;
            }
        }

        Vector<SnakePart> tail = getSnakeWithTail();
        for (int i = 0; i+1 < tail.size(); i++){
            if ((x == tail.get(i).getX()) && (y == tail.get(i).getY())){
                end = true;
                events.gameOver(getActualScore(),SnakeEvents.SNAKE_BIT_ITSELF);
                return false;
            }
        }

        return true;
    }

    private int getActualScore(){
        return (getLength()+shouldGrow);
    }

    int getOrientation() {
        return orientation;
    }

    Vector<WallBlock> getWallBlocks(){
        return wallBlocks;
    }

    Vector<SnakeEvent> getEvents(){
        return items;
    }


    Vector<SnakePart> getSnakeWithTail(){
        return parts;
    }

    private int getLength() {
        return getSnakeWithTail().size();
    }
}