package de.favo.game.snake;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class SettingsActivity extends AppCompatActivity {
    private SeekBar speedSeekBar, sizeSeekBar;
    private TextView speedTextField;
    private String[] speeds,speedsValues;
    private Switch vibrateSwitch, colorfulSwitch, gridSwitch, roundSwitch;
    private RadioButton[] wallRadioButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);
        speeds = getResources().getStringArray(R.array.pref_speed);
        speedsValues = getResources().getStringArray(R.array.pref_speed_real);

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        speedSeekBar = findViewById(R.id.settings_seek_bar_speed);
        sizeSeekBar = findViewById(R.id.settings_seek_bar_size);

        speedTextField = findViewById(R.id.settings_value_speed);

        colorfulSwitch = findViewById(R.id.settings_colorful_game);
        vibrateSwitch = findViewById(R.id.settings_vibrate);
        roundSwitch = findViewById(R.id.settings_round_snake);
        gridSwitch = findViewById(R.id.settings_show_grid);

        wallRadioButtons = new RadioButton[3];
        wallRadioButtons[0] = findViewById(R.id.settings_walls_buttons_none);
        wallRadioButtons[1] = findViewById(R.id.settings_walls_buttons_simple_line1);
        wallRadioButtons[2] = findViewById(R.id.settings_walls_buttons_simple_line2);

        wallRadioButtons[preferences.getInt("wallRadioButtons",0)].setChecked(true);
        vibrateSwitch.setChecked(preferences.getBoolean("vibrateSwitch",true));
        colorfulSwitch.setChecked(preferences.getBoolean("color_scheme",false));
        gridSwitch.setChecked(preferences.getBoolean("show_grid",true));
        roundSwitch.setChecked(preferences.getBoolean("roundSwitch",false));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            vibrateSwitch.setTrackResource(R.drawable.switch_track);
            vibrateSwitch.setThumbResource(R.drawable.switch_thumb);
            colorfulSwitch.setTrackResource(R.drawable.switch_track);
            colorfulSwitch.setThumbResource(R.drawable.switch_thumb);
            gridSwitch.setThumbResource(R.drawable.switch_thumb);
            gridSwitch.setTrackResource(R.drawable.switch_track);
            roundSwitch.setThumbResource(R.drawable.switch_thumb);
            roundSwitch.setTrackResource(R.drawable.switch_track);
        }

        for (int i = 0; i < wallRadioButtons.length; i++) {
            final int finalI = i;
            wallRadioButtons[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        SharedPreferences.Editor e = preferences.edit();
                        e.putInt("wallRadioButtons", finalI);
                        e.apply();
                    }
                }
            });
        }

        vibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor e = preferences.edit();
                e.putBoolean("vibrateSwitch",isChecked);
                e.apply();
            }
        });
        colorfulSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor e = preferences.edit();
                e.putBoolean("color_scheme",isChecked);
                e.apply();
            }
        });
        roundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor e = preferences.edit();
                e.putBoolean("roundSwitch",isChecked);
                e.apply();
            }
        });
       gridSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor e = preferences.edit();
                e.putBoolean("show_grid",isChecked);
                e.apply();
            }
        });

        speedSeekBar.setProgress(findPos(speedsValues,""+preferences.getInt("speedSeekBar",500)));
        sizeSeekBar.setProgress(preferences.getInt("block_size",10)-10);
        speedTextField.setText(speeds[speedSeekBar.getProgress()]);

        speedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SharedPreferences.Editor e = preferences.edit();
                e.putInt("speedSeekBar",Integer.parseInt(speedsValues[progress]));
                speedTextField.setText(speeds[progress]);
                e.apply();
            }@Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }@Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        sizeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SharedPreferences.Editor e = preferences.edit();
                e.putInt("block_size",progress+10);
                e.apply();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        OnSwipeTouchListener ostl = new OnSwipeTouchListener(this){
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                finish();
            }
        };

        findViewById(R.id.settings).setOnTouchListener(ostl);
        findViewById(R.id.settings_scroll).setOnTouchListener(ostl);

        GameHelpers.changeTTF(this,getWindow().getDecorView().getRootView());
    }

    private int findPos(Object[] array, Object item){
        for (int i = 0; i < array.length; i++)
            if (array[i].equals(item))
                return i;
        return -1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameHelpers.hideNavigations(this);
    }
}
