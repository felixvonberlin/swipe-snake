package de.favo.game.snake;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Button play = findViewById(R.id.play);
        Button scores = findViewById(R.id.highscores);
        Button settings = findViewById(R.id.settings);
        Button more = findViewById(R.id.more);

        play.setTypeface(GameHelpers.getBoldFont(this));
        scores.setTypeface(GameHelpers.getBoldFont(this));
        settings.setTypeface(GameHelpers.getBoldFont(this));
        ((TextView)findViewById(R.id.start_heading)).setTypeface(GameHelpers.getBoldFont(this));

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StartActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
        scores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StartActivity.this, HighScoreActivity.class);
                startActivity(i);
            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StartActivity.this, SettingsActivity.class);
                startActivity(i);
            }
        });
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(StartActivity.this, InfoActivity.class);
                startActivity(i);
            }
        });
        if (getSharedPreferences("SS",MODE_PRIVATE).getBoolean("first",true))
            startActivity(new Intent(getApplicationContext(), RightsActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameHelpers.hideNavigations(this);
    }

    @Override
    public void onBackPressed() {
    }
}
