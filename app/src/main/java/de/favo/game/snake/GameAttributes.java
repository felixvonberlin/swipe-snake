package de.favo.game.snake;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class GameAttributes{
    private int width;
    private int height;
    private SnakeEvents events;
    private int px_width;
    private int px_height;
    private int backgroundColor;
    private int foregroundColor;
    private int speed;
    private int wall;
    private boolean useColorFullMode;
    private boolean showGrid;
    private boolean makeSnakeRound;

    GameAttributes() {
    }

    GameAttributes setBlockWidth(int width) {
        this.width = width;
        return this;
    }

    GameAttributes setBlockHeight(int height) {
        this.height = height;
        return this;
    }


    SnakeEvents getEvents() {
        return events;
    }

    GameAttributes setEvents(SnakeEvents events) {
        this.events = events;
        return this;
    }

    int getPx_width() {
        return px_width;
    }

    GameAttributes setXSize(int px_width) {
        this.px_width = px_width;
        return this;
    }

    int getPx_height() {
        return px_height;
    }

    GameAttributes setYSize(int px_height) {
        this.px_height = px_height;
        return this;
    }
    int getBlockWidth(){
        return width;
    }
    int getBlockHeight(){
        return height;
    }
    int getXBlockCount(){
        return (int) Math.floor((double)(px_width / width));
    }
    int getYBlockCount(){
        return (int) Math.floor((double)(px_height / height));
    }

    int getBackgroundColor() {
        return backgroundColor;
    }

    int getForegroundColor() {
        return foregroundColor;
    }

    void setForegroundColor(int foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    int getSpeed() {
        return speed;
    }

    void setSpeed(int speed) {
        this.speed = speed;
    }

    boolean isUseColorFullMode() {
        return useColorFullMode;
    }

    void setUseColorFullMode(boolean useColorFullMode) {
        this.useColorFullMode = useColorFullMode;
    }

    int getWall() {
        return wall;
    }

    void setWall(int wall) {
        this.wall = wall;
    }

    boolean isShowGrid() {
        return showGrid;
    }

    void setShowGrid(boolean showGrid) {
        this.showGrid = showGrid;
    }

    boolean isMakeSnakeRound() {
        return makeSnakeRound;
    }

    void setMakeSnakeRound(boolean makeSnakeRound) {
        this.makeSnakeRound = makeSnakeRound;
    }
}
