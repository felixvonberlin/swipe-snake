package de.favo.game.snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder>{
    private List<HighScoreManager.HighScoreEntry> highscoreEntryList;
    private Context context;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, score, date;

        MyViewHolder(View view) {
            super(view);
            Typeface typeface = GameHelpers.getFont((context));
            title = view.findViewById(R.id.high_list_title);
            score = view.findViewById(R.id.high_list_score);
            date = view.findViewById(R.id.high_list_date);
            score.setTypeface(typeface);
            title.setTypeface(typeface);
            date.setTypeface(typeface);
            ((TextView)view.findViewById(R.id.high_list_description)).setTypeface(typeface);
        }
    }
    Adapter(Context c, List<HighScoreManager.HighScoreEntry> list) {
        this.highscoreEntryList = list;
        context = c;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_highscore, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HighScoreManager.HighScoreEntry entry = highscoreEntryList.get(position);
        holder.title.setText(entry.getName());
        holder.score.setText(entry.getScore() + "");
        holder.date.setText(
                new SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
                        .format(entry.getDate()));
    }
    @Override
    public int getItemCount() {
        return highscoreEntryList.size();
    }
}