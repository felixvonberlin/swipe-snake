package de.favo.game.snake;

/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class SnakePart {
    private int x;
    private int y;
    private int orientation;
    SnakePart(int x, int y, int orientation){
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    int getOrientation() {
        return orientation;
    }

    int getY() {
        return y;
    }

    int getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SnakePart snakePart = (SnakePart) o;

        if (x != snakePart.x) return false;
        if (y != snakePart.y) return false;
        return orientation == snakePart.orientation;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + orientation;
        return result;
    }
}
