package de.favo.game.snake;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import java.util.Locale;
/**
 *     Swipe Snake
 *     Copyright (C) 2017 Felix A. von Oertzen
 *     felix@von-oertzen-berlin.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        WebView webView = findViewById(R.id.info_web_view);
        webView.setBackgroundColor(Color.BLACK);
        webView.loadUrl(
                "file:///android_asset/html/about.html" +
                        (getString(R.string.lang).equals("de") ? ".de" : ".en"));
        findViewById(R.id.info).setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeRight() {
                finish();
            }
        });
        webView.setOnTouchListener(new OnSwipeTouchListener(this){
            @Override
            public void onSwipeRight() {
                finish();
            }
        });
        GameHelpers.changeTTF(this,getWindow().getDecorView().getRootView());
    }
}
